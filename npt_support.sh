#!/usr/bin/env vbash
# npt_support.sh
# Version: 1.0.0-0.1.b3
# Adds NPT support to EdgeOS 1.10.5
##### CONFIGURATION EXAMPLE #####
# NOTE: Create NPT attributes for each instance needed (npt01.. npt02, incrementing the number as necessary
# Interface
# set custom-attribute npt01_interface value tun1
# 
# User Local Addresses to translate from
# set custom-attribute npt01_ula value fd52:ffff:8011::/56
#
# Public Addresses to translate to
# set custom-attribute npt01_public value 2001:dddd:b845::/56
source /opt/vyatta/etc/functions/script-template

# Help documentation
help_docs() {
    cat <<HELP
Usage: npt_support.sh [command] [options]
Description: Adds or removes configured NPT instances
List of Commands:
    [none] - Applies configuration
    delete:    Remove all NPT rules
    update:    Re-apply configuration (WARNING: This causes a flap on ALL NPT instances)
HELP
}

# Basic Logger
# Logs various types of output with details
# Arguments: errorMessage [exitCode]
log() {
    prefix=$(date +'%F %H:%M:%S')

    case "${1}" in
        'debug') printf "\E[35m${prefix} - Debug: \n${2}\e[0m\n" ;;
        'info') printf "\E[36m${prefix} - Info: ${2}\e[0m\n" ;;
        'warning') printf "\E[33m${prefix} - Warning: ${2}\e[0m\n" ;;
        'error') 
            printf "\E[31m${prefix} - Error: ${2}\e[0m\nWould you like to continue (Y/[N])? "
            read keepGoing

            if [ "${keepGoing^^}" != 'Y' ]; then
                log info "Exiting on user cancel"
                exit "${3}"
            fi
        ;;
        'critical') 
            printf "\E[41m${prefix} - Critical Error: ${2}\e[0m\n"
            exit "${3}"
        ;;
    esac
}

# Change the IFS temporarily
OLDIFS="${IFS}"
IFS=$'\n'

# Rule handler
# Creates or deletes rules
# Note: No IPv6 NAT tables are used by EdgeOS as of 1.10.5, so this should be fairly reliable 
# Arguments: 
#   (add | update) interface pub ula
#   remove
modify_rules() {
    case "${1}" in 
        'add')
            if [[ "${#@}" ==  4 ]]; then
                pub="${3#\'}" && pub="${pub%\'}"
                ula="${4#\'}" && ula="${ula%\'}"

                log debug "ip6tables -t nat -A PREROUTING -i ${2} -j NETMAP -d ${pub} --to ${ula} -m comment --comment 'npt_support.sh: Network Prefix Translation'"
                ip6tables -t nat -A PREROUTING -i "${2}" -j NETMAP -d "${pub}" --to "${ula}" -m comment --comment 'npt_support.sh: Network Prefix Translation'
                log debug "ip6tables -t nat -A POSTROUTING -o ${2} -j NETMAP --to ${pub} -s ${ula} -m comment --comment 'npt_support.sh: Network Prefix Translation'"
                ip6tables -t nat -A POSTROUTING -o "${2}" -j NETMAP --to "${pub}" -s "${ula}" -m comment --comment 'npt_support.sh: Network Prefix Translation'
            else
                log debug "Parameters given:\n${@:1}"
                log critical 'Invalid configuration, missing parameters!' 5
            fi
        ;;
        'remove')
            log info 'Removing rules ...'
            ip6tables -t nat -F PREROUTING
            ip6tables -t nat -F POSTROUTING
            log info 'Complete!'
        ;;
        *)
            exit 10
        ;;
    esac
}

main() {
    if [[ "${#@}" == 0 ]]; then
        # Commands (this is faster, since we'll have a full config already)
        log info 'Reading configuration ...'
        configCommands=$(run show configuration commands)
        log info 'Complete!'

        for nptInstance in $(echo "${configCommands[@]}" | grep -oE 'npt[0-9]+' | uniq); do
            nptArgs=($(echo "${configCommands[@]}" | grep "${nptInstance}"))
            log info "Creating NPT instance ${nptInstance} ..."
            modify_rules add "${nptArgs[0]:43}" "${nptArgs[1]:40}" "${nptArgs[2]:37}"
            log info 'Complete!'
        done

    elif [[ "${1}" == 'delete' ]]; then
        modify_rules 'remove'
    elif [[ "${1}" == 'update' ]]; then
        modify_rules 'remove'
        main
    else 
        help_docs
        log critical 'Invalid command!' 1
    fi

    IFS="${OLDIFS}"
}

# GO!
main "${@}"